using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainCode : MonoBehaviour
{

    //scenaNum =1 => Rules
    //scenaNum =2 => Game
    //scenaNum =3 => EndGame
    //scenaNum =4 => Menu
    //scenaNum =5 => Option
    int scenaNum =0;

    public Button hammer;
    public Button drill;
    public Button masterKey;
    public Button[] startGame;
    public Button   exitMenu;

    public Text numPasOne;
    public Text numPasTwo;
    public Text numPasThree;

    private int pasOne;
    private int pasTwo;
    private int pasThree;
    string pasword;

    private int keyOne;
    private int keyTwo;
    private int keyThree;
    string key;

    public GameObject imCheckmarkOne;
    public GameObject imCheckmarkTwo;
    public GameObject imCheckmarkThree;
    public GameObject victory;
    public GameObject defeat;

    public GameObject menuCanvas;
    public GameObject gameCanvas;
    public GameObject endGameCanvas;
    public GameObject rulesCanvas;
    public GameObject optionCanvas;

    public Text timerText;
    public static float timerDown =60;

    public Text ScoreTime;
    //public Text BestScoreTime;

    private void Start()
    {
        timerText.text = timerDown.ToString();
        scenaNum = 4;
    }


    void Update()
    {
        Timer();
        SceneMamager();
    }


    public void SpawnPassword()
    {
        pasOne = Random.Range(1, 9);
        numPasOne.text = pasOne.ToString();
        pasTwo = Random.Range(1, 9);
        numPasTwo.text = pasTwo.ToString();
        pasThree = Random.Range(1, 9);
        numPasThree.text = pasThree.ToString();
        pasword = pasOne.ToString() + pasTwo.ToString() + pasThree.ToString();
    }


    public void SpawnKey()
    {
        if (scenaNum ==2)
        {
            keyOne = Random.Range(1, 9);
            keyTwo = Random.Range(1, 9);
            keyThree = Random.Range(1, 9);
            if (keyOne == pasOne)
            {
                keyOne = Random.Range(1, 9);
            }
            if (keyTwo == pasTwo)
            {
                keyTwo = Random.Range(1, 9);
            }
            if (keyThree == pasThree)
            {
                keyThree = Random.Range(1, 9);
            }
        }
        key = keyOne.ToString() + keyTwo.ToString() + keyThree.ToString();
        Debug.Log(key);
    }


    public void KeyAndPasswordComparison()
    {
        if (scenaNum == 2)
        {
            if (pasOne == keyOne)
            {
                imCheckmarkOne.SetActive(true);
            }
            else
            {
                imCheckmarkOne.SetActive(false);
            }
            if (pasTwo == keyTwo)
            {
                imCheckmarkTwo.SetActive(true);
            }
            else
            {
                imCheckmarkTwo.SetActive(false);
            }
            if (pasThree == keyThree)
            {
                imCheckmarkThree.SetActive(true);
            }
            else
            {
                imCheckmarkThree.SetActive(false);
            }
            if (key == pasword)
            {
                endGameCanvas.SetActive(true);
                ScoreTime.text = Mathf.Round(timerDown).ToString();
                scenaNum = 3;
                victory.SetActive(true);
                defeat.SetActive(false);
            }
        }
    }


    public void ClickHammer()
    {
        pasOne -= 1;
        pasTwo += 2;
        pasThree += 1;

        timerDown -= 1f;
    }


    public void ClickDrill()
    {
        pasOne += 2;
        pasTwo += 1;
        pasThree -= 2;

        timerDown -= 2f;
    }


    public void ClickMasterKey()
    {
        pasOne += 1;
        pasTwo -= 2;
        pasThree -= 1;
    }


    public void PrintPassword()
    {
        numPasOne.text = pasOne.ToString();
        numPasTwo.text = pasTwo.ToString();
        numPasThree.text = pasThree.ToString();
    }


    public void NumPasManager()
    {
        if (pasOne < 0)
        {
            pasOne = 9;
        }
        else if (pasOne > 9)
        {
            pasOne = 0;
        }

        if (pasTwo < 0)
        {
            pasTwo = 9;
        }
        else if (pasTwo > 9)
        {
            pasTwo = 0;
        }
        if (pasThree < 0)
        {
            pasThree = 9;
        }
        else if (pasThree > 9)
        {
            pasThree = 0;
        }
    }



    public void TestWinButton()
    {
        key = pasword;
    }


    public void SceneMamager()
    {
        if (scenaNum == 2)
        {
            if (timerDown < 0)
            {
                 scenaNum = 3;
                 defeat.SetActive(true);
                 victory.SetActive(false);
                 timerDown = 0;
            }
        }
         if (scenaNum == 3)
        {
            gameCanvas.SetActive(false);
            endGameCanvas.SetActive(true);
            timerDown = 0;
        }
         if (scenaNum == 4)
        {
            menuCanvas.SetActive(true);
            optionCanvas.SetActive(false);
            endGameCanvas.SetActive(false);
        }
        if (scenaNum == 5)
        {
            menuCanvas.SetActive(false);
            endGameCanvas.SetActive(false);
            optionCanvas.SetActive(true);
        }
    }


    public void StartRules()
    {
        scenaNum = 1;
        rulesCanvas.SetActive(true);
        menuCanvas.SetActive(false);
        endGameCanvas.SetActive(false);
    }


    public void StartGame()
    {
        timerDown = 60;
        scenaNum = 2;
        gameCanvas.SetActive(true);
        rulesCanvas.SetActive(false);
        endGameCanvas.SetActive(false);
    }


    public void ExitMenu()
    {
        scenaNum = 4;
    }


    public void ExiGame()
    {
        Application.Quit();
    }


    public void Option()
    {
        scenaNum = 5;
    }


    private void Timer()
    {
        if (scenaNum == 2)
        {
            timerDown -= Time.deltaTime;
            timerText.text = Mathf.Round(timerDown).ToString();
        }
    }


}